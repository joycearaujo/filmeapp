import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, TouchableHighlight, ImageBackground} from 'react-native';

export default class App extends Component{

  constructor(props){
    super(props);
    this.state={
      filmes: [],
      loading: true
    };

    // api https://sujeitoprogramador.com/rn-filmes/?api=filmes
  fetch('https://sujeitoprogramador.com/rn-filmes/?api=filmes')
  .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  },
  error => {
        var errmess = new Error(error.message);
        throw errmess;
  })
.then(response => this.setState({filmes: response, loading: false}))
.catch(error => alert(error));

  }

  render() {
    if(this.state.loading){
      return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: 18}}>Carregando ...</Text>
        </View>
      );
    }else{
      return (
        <View style={styles.container}>
  
          <View style={{height: 80, backgroundColor: '#007f98', justifyContent: 'center'}}>
            <Text style={{fontSize: 28, color: '#FFFFFF', fontWeight: 'bold', textAlign: 'center'}}>Filmes Recomendados</Text>
          </View>
  
          <FlatList
            data={this.state.filmes}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => { return <Filme data={item} /> }}
          />
  
        </View>
      );
    }
  }
}

class Filme extends Component{

  constructor(props){
    super(props);
  };

  render(){

    const { foto, nome } = this.props.data;

    return(
      <View style={styles.container}>
        <TouchableHighlight onPress={()=> alert("Filme: " + nome)} underlayColor="#CCCCCC">
          <ImageBackground resizeMode="cover" source={{uri: foto}} style={{height: 150}}>
            <View style={styles.areaNome}>
              <Text style={styles.textNome}>{nome}</Text>
            </View>
          </ImageBackground>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  textNome:{
    fontSize: 23,
    fontWeight: 'bold',
    color: '#FFFFFF'
  },
  areaNome:{
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    paddingLeft: 10,
    paddingBottom: 10
  }
});
